from gpiozero import LEDBoard
from gpiozero import LED
from time import sleep

star = LED(0)
tree = LEDBoard(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 16, 17, 18, 20, 21, 22, 23, 24, 25, 27)

while True:
    star.on()
    tree.on()
    sleep(1)

