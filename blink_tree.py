from gpiozero import LEDBoard
from gpiozero import PWMLED
from signal import pause

star = PWMLED(6)
branch1=LEDBoard(0,1,2,3,4,5)
branch2=LEDBoard(7,8,9,10,11,12)
branch3=LEDBoard(14,15,16,17,18, 20)
branch4= LEDBoard(21, 22, 23, 24, 25, 27)

a=0.33
b=0.45
c=0.57
d=0.63

star.pulse(fade_in_time=0.5, fade_out_time=0.5, background=True)
branch1.blink(on_time=a, off_time=a, background=True)
branch2.blink(on_time=b, off_time=b, background=True)
branch3.blink(on_time=c, off_time=c, background=True)
branch4.blink(on_time=d, off_time=d, background=True)
pause()

