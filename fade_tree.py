from gpiozero import LEDBoard
from gpiozero import PWMLED
from signal import pause

star = PWMLED(6)
tree = LEDBoard(0,1, 2, 3, 4, 5, 7, 8, 9, 10, 11, 12, 14, 15, 16, 17, 18, 20, 21, 22, 23, 24, 25, 27, pwm=True)

star.pulse(fade_in_time=1, fade_out_time=1, background=True)
tree.pulse(fade_in_time=2, fade_out_time=2, background=True)
pause()

