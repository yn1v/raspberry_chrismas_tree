from gpiozero import LED
from gpiozero import PWMLED
from signal import pause
from random import random

pines = [0, 1, 2, 3, 4, 5, 7, 8, 9, 10, 11, 12, 14, 15, 16, 17, 18, 20, 21, 22, 23, 24, 25, 27]

class LUCES:
    """ La clase luces instancia la variable self.tree
    donde se almacena el objeto LED, y le damos el valor del pin de
    GPIO y un random que se inicia con el init
    despues implemente un metodo "parpadeo" que simplemente hace una
    llamada a tree.blink.
    La teoria dice que deberia hacer las variables de la clase como
    variables privadas, en python no se pueden definir variables privadas
    pero existe la convencion de usar doble guion bajo __
    yo no lo use, pero por motivos de mantener el codigo limpio
    """
    def __init__ (self,led):
        """ Class initialiser """
        self.branch = LED(led)
        self.a = round(random(),2)
    def parpadeo (self):
        """ Function doc """
        self.branch.blink(on_time=self.a, off_time=self.a, background=True)

lista_luces=[] #aca almacenamos todos los objetos
for p in pines:
    luz = LUCES(p)
    lista_luces.append(luz)
	
star =  PWMLED(6)
star.pulse(fade_in_time=0.5, fade_out_time=0.5, background=True)

for luz_final in lista_luces:
    luz_final.parpadeo()

pause()
